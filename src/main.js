import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios';

import VueApexCharts from 'vue-apexcharts'

import tinymce from 'vue-tinymce-editor';

 import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
//Import Bootstrap an BootstrapVue CSS files (order is important)

 import 'bootstrap/dist/css/bootstrap.css'
 import 'bootstrap-vue/dist/bootstrap-vue.css'

import Toast from "vue-toastification";
import "vue-toastification/dist/index.css";

Vue.use(VueApexCharts)

Vue.use(router);

Vue.component('apexchart', VueApexCharts)

Vue.component('pagination', require('laravel-vue-pagination'));

Vue.use(Toast, {
  transition: "Vue-Toastification__bounce",
  maxToasts: 20,
  newestOnTop: true,
  position: "top-center",
  timeout: 5000,
  closeOnClick: true,
  pauseOnFocusLoss: true,
  pauseOnHover: true,
  draggable: true,
  draggablePercent: 0.6,
  showCloseButtonOnHover: false,
  hideProgressBar: true,
  closeButton: false,
  icon: true,
  rtl: false
});

// Make BootstrapVue available throughout your project
 Vue.use(BootstrapVue);

// Optionally install the BootstrapVue icon components plugin
 Vue.use(IconsPlugin);


Vue.component('tinymce', tinymce);


Vue.config.productionTip = false;



// axios.defaults.baseURL = 'http://41.59.227.124/gtp-api/public/api/'
// Vue.prototype.$remoteHost = 'http://41.59.227.124/gtp-api/public';

axios.defaults.baseURL = 'http://127.0.0.1:8004/api/';
Vue.prototype.$remoteHost = 'http://127.0.0.1:8004';

axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';


let token = localStorage.getItem('user-token');
if( token ){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
}
// axios.interceptors.request.use((config) => {
//   if (store.state.auth.token){ // or get it from localStorage
//     config.headers["Authorization"] = "Bearer "+ store.state.auth.token
//   }
//   return config
// })

const userPermissions = store.state.auth.user.permissions_assigned;

router.beforeEach((to, from, next) => {
    if (to.meta.permissions && to.meta.permissions.length > 0) {
        let isAllowed = userPermissions.some(p => to.meta.permissions.includes(p))
        
        if (! isAllowed) return next('/admin/denied')
    }

    next()
})


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
