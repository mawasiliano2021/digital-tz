import axios from 'axios'

export default {
    namespaced: true,
    state:{
       pagev:
       {
            page_id:null,
            visitor_ip:null,
       } 
    },

    mutations:{
        SET_IP (state, value) {
            state.pagev.visitor_ip = value
        },
        SET_PAGE (state, value) {
            state.pagev.page_id = value
        },
    },
    actions:{
        register({state}){
            return axios.post('pagecount',state.pagev).then(()=>{
            }).catch(()=>{
            })
        },

    }
    
}