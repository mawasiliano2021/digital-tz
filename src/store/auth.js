import axios from 'axios'
import router from '../router'

export default {
    namespaced: true,
    state:{
        authenticated:false,
        user:{},
        token:"",
        dashboard:{}
    },
    getters:{
        authenticated(state){
            return state.authenticated
        },
        user(state){
            return state.user
        },
        dashboard(state){
            return state.dashboard
        },
        token(state){
            return state.token
        },
    },
    mutations:{
        SET_AUTHENTICATED (state, value) {
            state.authenticated = value
        },
        SET_USER (state, value) {
            state.user = value
        },
        setToken(state, token){
            state.token = token;
        },
        DASHBOARD(state, dashboard){
            state.dashboard = dashboard;
        }
        
    },
    actions:{
        login({commit}){
            return axios.get('user').then(({data})=>{
                commit('SET_USER',data)
                commit('setToken',data)
                console.log(data);
                commit('SET_AUTHENTICATED',true)
                router.push('/admin/dashboard')
            }).catch(({response:{data}})=>{
                commit('SET_USER',data)
                commit('SET_AUTHENTICATED',false)
            })
        },
    dashboard({commit}){
       return  axios.get('dashboard')
      .then(response => {
        commit('DASHBOARD',response.data)
        console.log(response.data);
      })
      .catch(({response:{data}}) => {
        console.log(data)
        commit('DASHBOARD',data);
      })
    },
        logout({commit}){
            localStorage.removeItem('user-token')
            // remove the axios default header
            // delete axios.defaults.headers.common['Authorization']
            localStorage.setItem('user-token', "logout");
            commit('SET_USER',{})
            commit('SET_AUTHENTICATED',false)
        }
    }
    
}