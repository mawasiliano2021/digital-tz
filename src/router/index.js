import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Profile from '../views/admin/profile.vue'
import Dashboard from '../views/admin/dashboard.vue'
import Comment from '../views/admin/comment.vue'
import Page from '../views/admin/page.vue'
import Faqa from '../views/admin/faq.vue'
import User from '../views/admin/user.vue'
import viewPage from '../views/admin/view_page.vue'
import viewComment from '../views/admin/view_comment.vue'
import Service from '../views/service.vue'
import About from '../views/about.vue'
import Contact from '../views/contact.vue'
import Faq from '../views/faq.vue'
import Answers from '../views/answers.vue'
import grm from '../views/grm.vue'

import itemRegEd from '../views/admin/add_item.vue'
import faqRegEd from '../views/admin/add_faq.vue'

import tenders from '../views/tenders.vue'

import answerRegEd from '../views/admin/add_answer.vue'
import faqView from '../views/admin/view_faq.vue'
import uploadView from '../views/admin/upload.vue'
import viewDocument from '../views/admin/view_document.vue'
import addRole from '../views/admin/add_role.vue'
import editRole from '../views/admin/edit_role.vue'
import role from '../views/admin/role.vue'
import permission from '../views/admin/permission.vue'


import accessDenied from '../views/admin/access_denied.vue'



import Default from "../layouts/App.vue";
import Admin from "../layouts/Admin.vue";





Vue.use(VueRouter)

const routes = [
  {
    path: '',
    component: Default,
    children: [
      {
        path: '/',
        component: Home,
      },
      {
        path: '/home/profile',
        name: 'Profile',
        component: Profile
      },
      {
        path: '/home/service',
        name: 'Service',
        component: Service
      },
      {
        path: '/home/about',
        name: 'About',
        component: About
      },
      {
        path: '/home/faq',
        name: 'Faq',
        component: Faq
      },
      {
        path: '/home/tenders',
        name: 'tenders',
        component: tenders
      },
      {
        path: '/home/answers',
        name: 'Answers',
        component: Answers
      },
      {
        path: '/home/grm',
        name: 'GRM',
        component: grm
      },
      {
        path: '/home/contact',
        name: 'Contact',
        component: Contact
      }
    ],
  },
  {
    path: '',
    component: Admin,
    name: "dashboard-layout",
    children: [
      {
        path: '/admin/dashboard',
        component: Dashboard,
      },
      {
        path: '/admin/faq',
        component: Faqa,
      }, 
      {
        path: '/admin/user',
        component: User,
        meta: {
          // permissions: ['user_list']
         },
      },
      {
        path: '/admin/view/document/:id',
        component: viewDocument,
      },
      {
        path: '/admin/role',
        component: role,
        meta: {
          // permissions: ['role_list']
         },
      },
      {
        path: '/admin/permission',
        component: permission,
        meta: {
          // permissions: ['role_list']
         },
      },
      {
        path: '/admin/add_role',
        component: addRole,
      },
      {
        path: '/admin/edit_role/:id',
        component: editRole,
      },
      {
        path: '/admin/profile',
        component: Profile,
      },
      {
        path: '/admin/page',
        component: Page,
      },
      {
        path: '/admin/viewcomment/:id',
        component: viewComment
      },
      {
        path: '/admin/view_page/:id',
        component: viewPage
      },
      {
        path: '/admin/itemregedit/:id/:pageId',
        component: itemRegEd
      },
      {
        path: '/admin/answerregedit/:id/:commentId',
        component: answerRegEd
      },
      {
        path: '/admin/faqregedit/:id',
        component: faqRegEd
      },
      {
        path: '/admin/view_faq/:id',
        component: faqView
      },
      {
        path: '/admin/upload',
        component: uploadView,
        meta: {
          // permissions: ['document_list']
         },
      },
      {
        path: '/admin/comment',
        component: Comment,
      },
      {
        path: '/admin/denied',
        component: accessDenied,
      }
    ],
  },
]
const router = new VueRouter({
  mode: "hash",
 base: process.env.BASE_URL,
 routes
})



export default router
