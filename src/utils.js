
import moment from 'moment';

export default {

    formatDate: function(datettime){
        return  moment(datettime).format('DD/MM/YYYY hh:mm:ss');
    },
};