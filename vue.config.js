module.exports = {
    css: {
      loaderOptions: {
        sass: {
          additionalData: `
            @import "@/scss/_variables.scss";
          `
        }
      }
    },
    devServer: {
      disableHostCheck: true
    },
    publicPath: '',
  };
  